# LENIOLABS

  An app to apply to Leniolabs

## First run the following commands 
  ```
  bundle install
  rake db:create
  rake db:migrate
  ```
## If you wish you can run 
  ```
  rake db:seed
  ```
  if you run the seeds the admin user will have the email admin@admin.com and his password will be 123123
## Update cron tab
  ```
  whenever --update-crontab --set environment=development
  ```
## Common gems

  - pg : database postgresql gem
  - devise : authentication gem 
  - faker : gem to create recrods in seed file
  - simple_form : used in forms of the app
  - will_paginate, jquery-datatables-rails,jquery-rails : used in datatables

## Another gems 

  - cancancan : used for access to the app
  - rolify : roles for user
  - paranoia : deleted objects
  - whenever : to make cronjobs
  - carrierwave : to attach files in Comments

## Development gems
  
  - awesome_print : Better print of objects and console
  
## Test gems

  - rspec-rails
  - factory_bot_rails
  - database_cleaner

## Models

  ### Projects 
  project can have many tasks and  comments and belongs to an user 
  his name is unique and the kind of projects are 

    - normal
    - work
    - personal
    - family

  also this count with start and end which give the start and end dates of the project

  ### Task
  this model belongs to a project but is optional and can have many comments
  The status of task are :

     - init 
     - started
     - finish
     - near_to_expired : can be in this status only if have init or started status before
     - out_of_date : can be in this status only if have init or started status before

  Also counts with two methods 

      - before_expired : this method is used for change status of the tasks and only is runed
      like task(in libs) with whenever gem.
      - out_of_date : this method is used for change status of the tasks and only is runed
      like task(in libs) with whenever gem.

  ### User 
  An user can have many projects, tasks with through associations and many comments.
  extra things are :

    - rolify : for access in the project. and the default role is newuser

  ### Comment 
  this model belongs to an user and commentable as polymorphic. extra things are

      - DocumentUploader : to attach files with gem carrierwave
      - Paranoid : when a comment is deleted still an admin user can see them in the index comments
      view.

## VIEWS

  ### Comments
  in index only an admin user can see all comments an normal user can see only his comments. here we can see
  deleted comments from user because comment model have paranoid gem
  ### Projects
  in index we can see all projects with actions like delete, new, edit
  ### Tasks
  in index we can see all tasks with actions like delete, new, edit
  ### Users
  Only admin user can see this tab. here the users are showed and can be deleted

## API

  An little api with three controllers projects, comments and tasks. only with show and index actions.
  To request to the api add to the headers the X-API-KEY with the value 836ed3a72c75b1e2b06ca755adefd382f5faf3a some examples below

  ```
    http://localhost:3000/api/v1/projects/
    http://localhost:3000/api/v1/projects/1
  ```

## DEPLOYTED APP IN AMAZON EC2

  the site is avaible in the following ip http://3.16.21.33/ credentials
  are showed below. the app was deployed with unicorn and nginx in production

  ```
    user : user@admin.com
    password : 123123
    OR a normal user
    user : normal@user.com
    password : 123123
  ```

  anyway you can create more user in sign up