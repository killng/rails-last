class Tasks::CommentsController < CommentsController
  before_action :set_commentable
  
  def destroy
    @comment = Comment.find(params[:id])
    @comment = @comment.destroy
    respond_to do |format|
      if @comment.errors.present?
        puts ap @comment.errors.messages
        format.html { redirect_to task_url(@commentable.id) }
        format.json { head :no_content }
      else
        format.html { redirect_to task_url(@commentable.id)}
        format.json { head :no_content }
      end
    end
  end
  private
    def set_commentable
      @commentable = Task.find(params[:task_id])
    end
end
