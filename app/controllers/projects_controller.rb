class ProjectsController < ApplicationController
  
  before_action :set_project, only: [:edit, :update,:show, :destroy]
  def index
    current_user.has_role?(:admin) ? @projects = Project.all : @projects = current_user.projects
    respond_to do |format|
      format.html
      format.json { render json: ProjectsDatatable.new(view_context, @projects) }
    end
  end

  def show 
    # show task with priority
    @tasks = @project.tasks.order('priority DESC')
  end

  def edit
    @tasks = Task.where(project_id: nil)
  end
  
  def create
    @project = Project.new(project_params)
    @project.kind = Project.kinds.key(params[:kind].to_i)
    @tasks = Task.where(project_id: nil)
    @project.user_id = current_user.id
    respond_to do |format|
      if @project.save
        format.html { redirect_to projects_path }
      else
        @check_date =  true if @project.errors.full_messages.size == 0 # i know is not the better way but is for user can see
        format.html { render :new }
      end
    end
  end
  
  def new
    @project = Project.new
    @tasks = Task.where(project_id: nil)
  end
  
  def update
    respond_to do |format|
      new_task = Task.find(params[:task_id]) if params.has_key?(:task_id) && !params[:task_id].blank? 
      @project.tasks = @project.tasks.push(new_task) if !new_task.nil?
      if @project.update(project_params)  && @project.update_column(:kind, params[:kind])
        if !new_task.nil?
          new_task.update_column(:project_id, @project.id)
        end
        format.html { redirect_to projects_path }
      else
        format.html { render :edit }
      end
    end
  end


  def destroy
    @project = @project.destroy 
    respond_to do |format|
      if @project.errors.present?
        puts ap @project.errors.messages
        format.html { redirect_to projects_url }
        format.json { head :no_content }
      else
        format.html { redirect_to projects_url}
        format.json { head :no_content }
      end
    end
  end

  private
  def set_project
    @project = Project.find(params[:id])
  end

  def project_params
    params.require(:project)
          .permit(:name, :start, :end, :user_id, :description, :kind)
  end

end
