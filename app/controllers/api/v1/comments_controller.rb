module Api
  module V1
    class CommentsController < V1Controller
      before_action :set_comment, only: [:show]

      def index
        render json: { status: :ok, comments: Comment.all}
      end
      
      def show
        render json: { status: :ok, comment: @comment}
      end

      private
      def set_comment
        @comment = Comment.find(params[:id])
      end

    end
  end
end


