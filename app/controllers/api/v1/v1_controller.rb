module Api
	class V1::V1Controller < ApiController
		before_action :check_api_key
		#before_action :check_auth_token

    def check_api_key
      if request.headers['X-API-KEY'] != '836ed3a72c75b1e2b06ca755adefd382f5faf3a'
        return render json: { error: 'API Token inválido.' }, status: :bad_request
      end
    end

    def check_auth_token
      if request.headers['X-Auth-Token'].present?
        @user = User.find_by(mobile_token: request.headers['X-Auth-Token'])
        unless @user.present?
          return render json: { error: 'Usuario no encontrado', status: :unauthorized }, status: :bad_request
        end
      else
        return render json: { error: 'Token no encontrado' }, status: :bad_request
      end

    end

  end
end
