class Task < ApplicationRecord
  belongs_to :project, optional: true
  has_many :comments, as: :commentable, dependent: :destroy

  validates :name, :priority, :status, :deadline_days, presence: true
  # a task can be in status out_of_date only if have init or started status before
  enum status: [:init, :started, :finish, :near_to_expired, :out_of_date]

  scope :init_tasks, -> { where(status: "init") }
  scope :terminated, -> { where("status = ? OR status = ? OR status = ?", 2, 3, 4) }

  def self.out_of_date 
    tasks = Task.where("status = ? OR status = ?", 0, 1 ) # only tasks with status init or started
    tasks.each do |t|
      !t.end_date.nil?  && t.end_date < Time.now ? t.update_column(:status, "out_of_date") : ''
    end
  end

  def self.before_expired
    tasks = Task.where("status = ? OR status = ?", 0, 1 ) # only tasks with status init or started
    tasks.each do |t|
      # check if is end_date is between time.now with deadline_days
      !t.deadline_days.nil? && t.deadline_days > 0 && !t.end_date.nil?  && t.end_date < Time.now && t.end_date > Time.now - t.deadline_days.days ? t.update_column(:status, "near_to_expired") : ''
    end
  end

end
