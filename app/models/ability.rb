class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.has_role?(:admin)
      can :manage, :all
    elsif user.has_role?(:new_user)
      can :index, Task
      can [:show, :edit], Task, user_id: user.id
    end
  end
end
