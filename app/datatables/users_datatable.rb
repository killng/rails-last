class UsersDatatable
  delegate :params, :h, :link_to, to: :@view

  include Rails.application.routes.url_helpers
  include ActionView::Helpers::OutputSafetyHelper

  def initialize(view,data)
    @view = view
    @users = data
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: User.count,
      iTotalDisplayRecords: @users.size,
      aaData: data
    }
  end

  private

  def data
    users.map do |user|
      delete_user = "¿Are you sure want to delete this user #{user.name}?"
      [
        (user.name.present? ? user.name :  ''),
        (user.email.present? ? user.email : '' ),
        (user.created_at.present? ? user.created_at.strftime("%d-%m-%Y") : '' )
        #(user.end.present? ? user.end.strftime("%d-%m-%Y") : '' ),
        #(user.end.present? && user.end < Time.now ? 'Yes': 'No' ),
        #link_to('Delete', user_path(user), method: :delete, data: { confirm: delete_user }, class: 'btn btn-xs btn-danger') #+" "+
        #link_to('Show', user_path(user), class: 'btn btn-xs btn-success')+" "+
        #link_to('Edit', edit_user_path(user), class: 'btn btn-xs btn-warning')
      ]
    end
  end

  def users
    @users = fetch_users
  end

  def fetch_users
    users = @users.order("#{sort_column} #{sort_direction}")
    users = users.page(page).per_page(per_page)   
    if params[:sSearch].present?
      params[:sSearch] = params[:sSearch].downcase
      users = User.where("CAST(TO_CHAR(users.created_at, 'dd-mm-YYYY') AS TEXT) LIKE :search OR LOWER(users.name) LIKE :search OR LOWER(users.email) LIKE :search ", search: "%#{params[:sSearch]}%")
    end
    users
  end

  def page
    params[:iDisplayStart].to_i / per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = [ 'users.name', 'users.email', 'users.created_at', '']
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == 'desc' ? 'asc' : 'desc'
  end
end
