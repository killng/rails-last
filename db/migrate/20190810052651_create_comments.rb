class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.text :body, null: false
      t.references :user, foreign_key: true
      t.string :commentable_type, null: false
      t.integer :commentable_id, index: true, null: false

      t.timestamps
    end
  end
end
