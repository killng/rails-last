class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.string :name, null: false
      t.references :project, foreign_key: true
      t.integer :priority, default: 0, null: false
      t.integer :status, default: 0, null: false
      t.datetime :end_date
      t.integer :deadline_days, default: 0, null: false

      t.timestamps
    end
  end
end
