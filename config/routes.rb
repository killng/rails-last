Rails.application.routes.draw do
  devise_for :users
  resources :projects do
    resources :comments, only: [:destroy,:new,:index,:create], module: :projects
  end
  resources :users, only: [:show,:index]
  resources :comments, only: [:show,:index,:destroy]
  resources :tasks do
    resources :comments, only: [:destroy,:new,:index,:create], module: :tasks
  end
  devise_scope :user do
	  authenticated :user do
	    root 'projects#index', as: :authenticated_root
	  end
		unauthenticated do
	   root "devise/sessions#new", as: :unauthenticated_root
	  end
  end
  ## API
  namespace :api , defaults: { format: 'json' } do
    namespace :v1 do
      resources :tasks, only: [:show,:index]
      resources :projects, only: [:show,:index]
      resources :comments, only: [:show,:index]
    end
  end
end
