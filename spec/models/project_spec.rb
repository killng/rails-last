require 'rails_helper'

RSpec.describe Project, type: :model do
  it "has none to begin with" do
    expect(Project.count).to eq 0
  end
  it "has 0 after adding with blank params" do
    Project.create
    expect(Project.count).to eq 0
  end
  
  #it "has 1 after adding with email" do
  #  Project.create(name: "#{Faker::Space.planet}",start: Time.now, user_id: 1)
  #  expect(Project.count).to eq 1
  #end

  #it "has to be valid" do
  #  @Project1 = create(:Project)
  #  expect(@Project1).to be_valid
  #end


end
