require 'rails_helper'

RSpec.describe User, type: :model do
  it "has none to begin with" do
    expect(User.count).to eq 0
  end
  it "has 0 after adding with blank params" do
    User.create
    expect(User.count).to eq 0
  end
  #it "has 1 after adding with email" do
  #  User.create(email: "g@g.com", password: "123123")
  #  expect(User.count).to eq 1
  #end

  #it "has to be valid" do
  #  @user1 = create(:user)
  #  expect(@user1).to be_valid
  #end


end
